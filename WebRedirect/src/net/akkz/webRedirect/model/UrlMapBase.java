package net.akkz.webRedirect.model;

public abstract class UrlMapBase
{
	public abstract int getId();
	
	public abstract void setId(int id);
	
	public abstract String getUrl();

	public abstract void setUrl(String url);

	public abstract String getMap();

	public abstract void setMap(String map);
	
	public abstract int getState();
	
	public abstract void setState(int state);
}