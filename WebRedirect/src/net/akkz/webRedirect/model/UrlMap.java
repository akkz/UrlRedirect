package net.akkz.webRedirect.model;

public class UrlMap extends UrlMapBase
{
	private int id;
	private String url;
	private String map;
	
	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}

	public String getMap()
	{
		return map;
	}

	public void setMap(String map)
	{
		this.map = map;
	}

	@Override
	public int getState()
	{
		return 0;
	}

	@Override
	public void setState(int state)
	{
	}

}
