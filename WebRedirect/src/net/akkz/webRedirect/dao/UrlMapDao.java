package net.akkz.webRedirect.dao;

import java.util.List;

import net.akkz.dbFactory.Session;
import net.akkz.exception.BuildClassError;
import net.akkz.exception.SQLExcuteException;
import net.akkz.webRedirect.model.UrlMapBase;;

public interface UrlMapDao
{
	public void addUrlMap(Session session, UrlMapBase urlMap) throws SQLExcuteException;
	
	public void deleteUrlMap(Session session,UrlMapBase urlMap) throws SQLExcuteException;
	
	public void upateUrlMap(Session session,UrlMapBase urlMap) throws SQLExcuteException;
	
	public UrlMapBase getUrlMap(Session session,int id) throws SQLExcuteException, BuildClassError;
	
	public List<UrlMapBase> getUrlMap(Session session,String url) throws SQLExcuteException, BuildClassError;
}
