package net.akkz.webRedirect.dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import net.akkz.dbFactory.Session;
import net.akkz.exception.BuildClassError;
import net.akkz.exception.SQLExcuteException;
import net.akkz.webRedirect.model.UrlMap;
import net.akkz.webRedirect.model.UrlMapBase;

public class UrlMapDaoFileImpl implements UrlMapDao
{
	private List<UrlMapBase> maps;

	public UrlMapDaoFileImpl()
	{
		this.readMaps();
	}

	@Override
	public void addUrlMap(Session session, UrlMapBase urlMap) throws SQLExcuteException
	{
		urlMap.setId(maps.size());
		this.maps.add(urlMap);
		
		this.writeMaps();
	}

	@Override
	public void deleteUrlMap(Session session, UrlMapBase urlMap) throws SQLExcuteException
	{
		for(int i=0; i<maps.size(); i++)
		{
			if(maps.get(i).getId() == urlMap.getId())
			{
				maps.remove(i);
				return;
			}
		}
		
		this.writeMaps();
	}

	@Override
	public void upateUrlMap(Session session, UrlMapBase urlMap) throws SQLExcuteException
	{
		for(int i=0; i<maps.size(); i++)
		{
			if(maps.get(i).getId() == urlMap.getId())
			{
				UrlMapBase u = maps.get(i);
				u.setMap(urlMap.getMap());
				u.setUrl(urlMap.getUrl());
				
				return;
			}
		}
		
		this.writeMaps();
	}

	@Override
	public UrlMapBase getUrlMap(Session session, int id) throws SQLExcuteException, BuildClassError
	{
		return maps.get(id);
	}

	@Override
	public List<UrlMapBase> getUrlMap(Session session, String url) throws SQLExcuteException, BuildClassError
	{
		List<UrlMapBase> urlMaps = new ArrayList<UrlMapBase>();
		
		for(int i=0; i<maps.size(); i++)
		{
			if(maps.get(i).getUrl().equals(url))
			{
				urlMaps.add( maps.get(i));
			}
		}
		
		return urlMaps;
	}

	private void writeMaps()
	{
		try
		{
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(this.getClass().getClassLoader().getResource("").getPath() + "ips.conf"))));
			
			for(UrlMapBase u : maps)
			{
				bw.write(u.getId() + " " + u.getUrl() + " " + u.getMap());
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	private void readMaps()
	{
		this.maps = new ArrayList<UrlMapBase>();
		
		try
		{
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(new File(this.getClass().getClassLoader().getResource("").getPath() + "ips.conf"))));

			String str;
			while((str=br.readLine()) != null)
			{
				String[] t = str.split(" ");
				
				if(t.length == 3)
				{
					UrlMap u = new UrlMap();
					u.setId(Integer.parseInt(t[0]));
					u.setUrl(t[1]);
					u.setMap(t[2]);
					
					maps.add(u);
				}
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
