package net.akkz.webRedirect.dao;

import java.util.ArrayList;
import java.util.List;

import net.akkz.dbFactory.Session;
import net.akkz.exception.BuildClassError;
import net.akkz.exception.SQLExcuteException;
import net.akkz.webRedirect.model.UrlMap;
import net.akkz.webRedirect.model.UrlMapBase;

public class UrlMapDaoImpl implements UrlMapDao
{

	@Override
	public void addUrlMap(Session session,UrlMapBase urlMap) throws SQLExcuteException
	{
		String sql = "insert into maps(url, map) values(?, ?)";
		
		String[] values = { urlMap.getUrl(), urlMap.getMap() };
		
		session.excute(sql, values);
	}

	@Override
	public void deleteUrlMap(Session session,UrlMapBase urlMap) throws SQLExcuteException
	{
		String sql = "delete from maps where id = ?";
		String[] values = {String.valueOf(urlMap.getId())};
		
		session.excute(sql, values);
	}

	@Override
	public void upateUrlMap(Session session,UrlMapBase urlMap) throws SQLExcuteException
	{
		String sql = "update maps set url = ?, map = ? where id = ?";
		String[] values = {urlMap.getUrl(), urlMap.getMap(), String.valueOf(urlMap.getId())};
		
		session.excute(sql, values);
	}

	@Override
	public UrlMap getUrlMap(Session session,int id) throws SQLExcuteException, BuildClassError
	{
		String sql = "select * from maps where id = ?";
		
		return session.get(sql, id, UrlMap.class);
	}

	@Override
	public List<UrlMapBase> getUrlMap(Session session,String url) throws SQLExcuteException, BuildClassError
	{
		String sql = "select * from maps where url = ?";
		String[] values = { url };
		
		List<UrlMapBase> returns = new ArrayList<UrlMapBase>();
		for(UrlMapBase u : session.find(sql, values, UrlMap.class))
		{
			returns.add(u);
		}
		
		return returns;
	}

}
