package net.akkz.webRedirect.service;

import java.util.List;

import net.akkz.dbFactory.DbManage;
import net.akkz.webRedirect.dao.UrlMapDao;
import net.akkz.webRedirect.model.UrlMapBase;

public class UrlMapServiceFileImpl implements UrlMapService
{
	private UrlMapDao urlMapDao;
	private DbManage dbManage;

	@Override
	public String getMap(String url)
	{
		try
		{
			List<UrlMapBase> turl = urlMapDao.getUrlMap(null, url);

			if (turl.size() != 0)
				return turl.get((int) (turl.size() * Math.random())).getMap();
			else
				return null;
		}
		catch (Exception e)
		{
			e.printStackTrace();

			return null;
		}
	}

	public UrlMapDao getUrlMapDao()
	{
		return urlMapDao;
	}

	public void setUrlMapDao(UrlMapDao urlMapDao)
	{
		this.urlMapDao = urlMapDao;
	}

	public DbManage getDbManage()
	{
		return dbManage;
	}

	public void setDbManage(DbManage dbManage)
	{
		this.dbManage = dbManage;
	}

	@Override
	public UrlMapBase getUrlMap(String url)
	{
		try
		{
			List<UrlMapBase> turl = urlMapDao.getUrlMap(null, url);

			if (turl.size() != 0)
				return turl.get((int) (turl.size() * Math.random()));
			else
				return null;
		}
		catch (Exception e)
		{
			e.printStackTrace();

			return null;
		}
	}

}
