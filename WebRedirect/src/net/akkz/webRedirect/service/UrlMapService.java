package net.akkz.webRedirect.service;

import net.akkz.webRedirect.model.UrlMapBase;

public interface UrlMapService
{
	public String getMap(String url);
	
	public UrlMapBase getUrlMap(String url);
}
