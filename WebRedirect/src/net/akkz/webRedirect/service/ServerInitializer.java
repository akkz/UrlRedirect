package net.akkz.webRedirect.service;

import javax.servlet.*;
import javax.servlet.annotation.WebListener;

import net.akkz.beanFactory.BeanFactory;


@WebListener
public class ServerInitializer implements ServletContextListener
{
	public ServerInitializer()
	{
		// TODO Auto-generated constructor stub
	}

	public void contextInitialized(ServletContextEvent sec)
	{
		ServletContext context = sec.getServletContext();
		
		BeanFactory beanFactory = new BeanFactory();
		beanFactory.init();
		
		context.setAttribute("beanFactory", beanFactory);
	}

	public void contextDestroyed(ServletContextEvent arg0)
	{
		// TODO Auto-generated method stub
	}

}
