package net.akkz.webRedirect.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class BaseServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	private BeanFactory beanFactory;

	public void setBeanFactory(BeanFactory beanFactory)
	{
		this.beanFactory = beanFactory;
	}

	public BeanFactory getBeanFactory()
	{
		return beanFactory;
	}

	public BaseServlet()
	{
		super();
	}

	protected abstract void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

	protected abstract void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;
}
