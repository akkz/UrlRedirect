package net.akkz.webRedirect.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.akkz.beanFactory.BeanFactory;
import net.akkz.webRedirect.model.UrlMapBase;
import net.akkz.webRedirect.service.UrlMapService;

/**
 * Servlet implementation class DefaultRedirect
 */
@WebServlet(name="HideRedirect", urlPatterns="/hideRedirect.do")
public class HideRedirect extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	private UrlMapService urlMapService;

	/**
	 * Default constructor.
	 */
	public HideRedirect()
	{
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		doRedirect(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		doRedirect(request, response);
	}

	
	private void doRedirect(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String url = request.getRequestURL().toString();
		
		int s = url.indexOf("/");
		url = url.substring(s+2);

		s = url.indexOf("/");
		url = url.substring(0, s);
		
		UrlMapBase map = urlMapService.getUrlMap(url);
		
		if(map == null)
		{
			map  = urlMapService.getUrlMap("default");
		}
		
		if(map == null)
		{
			response.sendRedirect("/jsp/errorPage.html");
			
			return;
		}

		request.setAttribute("map", map.getMap());
		
		if(map.getState() == 0)
			request.getRequestDispatcher("/jsp/reponseUrl.jsp").forward(request, response);
		else
			request.getRequestDispatcher("/jsp/reponseWithHide.jsp").forward(request, response);
	}

	@Override
	public void init()
	{
		BeanFactory  bf = (BeanFactory) this.getServletContext().getAttribute("beanFactory");
		
		urlMapService = bf.getBean("urlMapService");
	}
}
